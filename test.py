import sequences, values

def run():
    tests = [
             (sequences.fibonacci, 20),
             (sequences.primes, 20),
             (sequences.naturalsquares, 20),
             (sequences.mersenne, 8),
             (values.fibonacci, 33),
             (values.pi, 10),
             (values.pi, 10000),
             (values.factorial, 5),
             (values.factorial, 10)
             ]

    for func, value in tests:
        print("Running {}.{} : {} ".format(func.__module__, func.__name__, func.__doc__))
        try:
            print("{}({}) = {}".format(func.__name__, value, func(value)))
        except Exception as e:
            print("Error: {}".format(e))
        print('')

if __name__ == '__main__':
    run()
